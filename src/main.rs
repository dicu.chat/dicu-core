use crate::db::types::{Answers, Context, PermissionLevel, User};
use crate::db::Database;
use maplit::hashmap;
use std::default::Default;
use tokio_stream::StreamExt;

mod db;

// To run, need to set environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
#[tokio::main]
async fn main() {
    let db = db::Database::new("http://0.0.0.0:8000", "global");

    dbg!(db.create_user_table().await);
    dbg!(db.create_checkbacks_table().await);
    dbg!(db.create_access_tokens_table().await);
    dbg!(db.create_sessions_table().await);
    dbg!(db.create_string_items_table().await);
    dbg!(db.create_external_services_table().await);
}
