//! Types to ensure the correct types are used for each User attribute. And also that the attribute
//! name is not misspelled.
// TODO: Autogenerate this using macros

use serde::Serialize;
use std::collections::HashMap;

pub trait UserAttribute {
    const NAME: &'static str;
    type Type: Serialize;
}

pub enum UserId {}
pub enum Service {}
pub enum ViaInstance {}
pub enum FirstName {}
pub enum LastName {}
pub enum Username {}
pub enum Language {}
pub enum CreatedAt {}
pub enum Location {}
pub enum LastActive {}
pub enum Conversation {}
pub enum Answers {}
pub enum Files {}
pub enum States {}
pub enum Permissions {}
pub enum Context {}

impl UserAttribute for UserId {
    const NAME: &'static str = "user_id";
    type Type = String;
}

impl UserAttribute for Service {
    const NAME: &'static str = "service";
    type Type = String;
}

impl UserAttribute for ViaInstance {
    const NAME: &'static str = "via_instance";
    type Type = String;
}

impl UserAttribute for FirstName {
    const NAME: &'static str = "first_name";
    type Type = Option<String>;
}

impl UserAttribute for LastName {
    const NAME: &'static str = "last_name";
    type Type = Option<String>;
}

impl UserAttribute for Username {
    const NAME: &'static str = "username";
    type Type = Option<String>;
}

impl UserAttribute for Language {
    const NAME: &'static str = "language";
    type Type = String;
}

impl UserAttribute for CreatedAt {
    const NAME: &'static str = "created_at";
    type Type = String;
}

impl UserAttribute for Location {
    const NAME: &'static str = "location";
    type Type = Option<String>;
}

impl UserAttribute for LastActive {
    const NAME: &'static str = "last_active";
    type Type = String;
}

impl UserAttribute for Conversation {
    const NAME: &'static str = "conversation";
    type Type = Option<String>;
}

impl UserAttribute for Answers {
    const NAME: &'static str = "answers";
    type Type = crate::db::types::Answers;
}

impl UserAttribute for Files {
    const NAME: &'static str = "files";
    type Type = HashMap<String, String>;
}

impl UserAttribute for States {
    const NAME: &'static str = "states";
    type Type = Vec<String>;
}

impl UserAttribute for Permissions {
    const NAME: &'static str = "permissions";
    type Type = crate::db::types::PermissionLevel;
}

impl UserAttribute for Context {
    const NAME: &'static str = "context";
    type Type = crate::db::types::Context;
}
