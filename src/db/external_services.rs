use crate::db::types::ExternalService;
use crate::db::{Database, DbResult};
use maplit::hashmap;
use rusoto_core::RusotoError;
use rusoto_dynamodb::{
    AttributeDefinition, CreateTableError, CreateTableInput, DynamoDb, GetItemError,
    KeySchemaElement, ProvisionedThroughput, PutItemError, ScanError,
};
use serde_dynamo::to_attribute_value;
use tokio_stream::Stream;

impl Database {
    pub(crate) const EXTERNAL_SERVICES_TABLE: &'static str = "ExternalServices";

    /// Creates the external services table with identity and service as the key.
    /// Boolean indicates if the table was created.
    pub async fn create_external_services_table(&self) -> DbResult<bool, CreateTableError> {
        let res = self
            .dynamo
            .create_table(CreateTableInput {
                table_name: Self::EXTERNAL_SERVICES_TABLE.to_string(),
                attribute_definitions: vec![
                    AttributeDefinition {
                        attribute_name: "identity".to_string(),
                        attribute_type: "S".to_string(),
                    },
                    AttributeDefinition {
                        attribute_name: "service".to_string(),
                        attribute_type: "S".to_string(),
                    },
                ],

                key_schema: vec![
                    KeySchemaElement {
                        attribute_name: "identity".to_string(),
                        key_type: "HASH".to_string(),
                    },
                    KeySchemaElement {
                        attribute_name: "service".to_string(),
                        key_type: "RANGE".to_string(),
                    },
                ],

                global_secondary_indexes: None,

                provisioned_throughput: Some(ProvisionedThroughput {
                    read_capacity_units: 5000,
                    write_capacity_units: 5000,
                }),
                ..Default::default()
            })
            .await;

        match res {
            Ok(_) => Ok(true),
            // If it failed because the table already exists, lets not return an error.
            Err(RusotoError::Service(CreateTableError::ResourceInUse(_))) => Ok(false),
            Err(e) => Err(e.into()),
        }
    }

    /// Puts an external service into the database, overwriting any with the same key.
    /// Returns any overwritten item
    pub async fn put_external_service(
        &self,
        item: &ExternalService,
    ) -> DbResult<Option<ExternalService>, PutItemError> {
        self.external_services.put(item).await
    }

    /// Gets the external service with the given identity and service if found.
    pub async fn get_external_service(
        &self,
        identity: &str,
        service: &str,
    ) -> DbResult<Option<ExternalService>, GetItemError> {
        let key = hashmap! {
            "identity".to_string() => to_attribute_value(identity).unwrap(),
            "service".to_string() => to_attribute_value(service).unwrap(),
        };

        self.external_services.get(key).await
    }

    /// Iterates through all external services
    pub fn iter_external_services(
        &self,
    ) -> impl Stream<Item=DbResult<ExternalService, ScanError>> + '_ {
        self.external_services.iter_entries(None, None)
    }
}
