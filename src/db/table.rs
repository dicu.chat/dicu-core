use crate::db::DbResult;
use async_stream::try_stream;
use maplit::hashmap;
use rusoto_dynamodb::{
    BatchGetItemError, BatchGetItemInput, BatchWriteItemError, BatchWriteItemInput,
    DeleteItemError, DeleteItemInput, DynamoDb, DynamoDbClient, GetItemError, GetItemInput,
    KeysAndAttributes, PutItemError, PutItemInput, PutRequest, ScanError, ScanInput, ScanOutput,
    UpdateItemError, UpdateItemInput, WriteRequest,
};
use serde::de::DeserializeOwned;
use serde::Serialize;
use serde_dynamo::{from_item, to_item, Item};
use std::default::Default;
use std::marker::PhantomData;
use tokio_stream::Stream;

/// A wrapper around DynamoDbClient that only interfaces with one table and only returns that
/// table's type.
pub(crate) struct Table<T: Serialize + DeserializeOwned> {
    dynamo: DynamoDbClient,
    table_name: &'static str,
    _phantom: PhantomData<T>,
}

impl<T: Serialize + DeserializeOwned> Table<T> {
    pub fn new(db: DynamoDbClient, table: &'static str) -> Table<T> {
        Table {
            dynamo: db,
            table_name: table,
            _phantom: PhantomData,
        }
    }

    /// Put the entry into the database. If an entry is overwritten, the old entry is returned.
    pub async fn put(&self, item: &T) -> DbResult<Option<T>, PutItemError> {
        let output = self
            .dynamo
            .put_item(PutItemInput {
                table_name: self.table_name.to_string(),
                item: to_item(item).unwrap(),
                return_values: Some("ALL_OLD".to_string()),
                ..PutItemInput::default()
            })
            .await?;

        Ok(match output.attributes {
            Some(e) => from_item(e)?,
            None => None,
        })
    }

    /// Puts many entries into the database. Returning any that were not processed.
    pub async fn batch_put(&self, items: &[T]) -> DbResult<Option<Vec<T>>, BatchWriteItemError> {
        let mut write_requests = Vec::with_capacity(items.len());
        for item in items {
            write_requests.push(WriteRequest {
                delete_request: None,
                put_request: Some(PutRequest {
                    item: to_item(item)?,
                }),
            });
        }

        let request_items = hashmap! {
            self.table_name.to_string() => write_requests
        };

        let output = self
            .dynamo
            .batch_write_item(BatchWriteItemInput {
                request_items,
                ..Default::default()
            })
            .await?;

        if let Some(mut unprocessed) = output.unprocessed_items {
            let requests = unprocessed.remove(self.table_name).unwrap();
            let mut unprocessed_items = Vec::with_capacity(requests.len());

            for req in requests {
                unprocessed_items.push(from_item(req.put_request.unwrap().item).unwrap());
            }

            Ok(Some(unprocessed_items))
        } else {
            Ok(None)
        }
    }

    /// Delete the entry from the database with the given key. If an entry is deleted, it is
    /// returned.
    pub async fn delete(&self, key: Item) -> DbResult<Option<T>, DeleteItemError> {
        let output = self
            .dynamo
            .delete_item(DeleteItemInput {
                table_name: self.table_name.to_string(),
                key,
                return_values: Some("ALL_OLD".to_string()),
                ..DeleteItemInput::default()
            })
            .await?;

        Ok(match output.attributes {
            Some(e) => from_item(e)?,
            None => None,
        })
    }

    /// Get the entry from the database with the given key. If an entry exists with that key, it is
    /// returned.
    pub async fn get(&self, key: Item) -> DbResult<Option<T>, GetItemError> {
        let output = self
            .dynamo
            .get_item(GetItemInput {
                table_name: self.table_name.to_string(),
                key,
                ..GetItemInput::default()
            })
            .await?;

        // If get_item succeeded, return the item if it is there, or None
        Ok(match output.item {
            Some(e) => from_item(e)?,
            None => None,
        })
    }

    /// Try and get many entries at once. Returns a tuple of responses and unprocessed keys.
    pub async fn batch_get(
        &self,
        keys: Vec<Item>,
    ) -> DbResult<(Option<Vec<T>>, Option<Vec<Item>>), BatchGetItemError> {
        let keys_and_attr = KeysAndAttributes {
            keys,
            ..Default::default()
        };

        let request_items = hashmap! {
            self.table_name.to_string() => keys_and_attr
        };

        let output = self
            .dynamo
            .batch_get_item(BatchGetItemInput {
                request_items,
                ..Default::default()
            })
            .await?;

        let responses = if let Some(mut responses_map) = output.responses {
            let responses = responses_map.remove(self.table_name).unwrap();
            let mut responses_items = Vec::with_capacity(responses.len());

            for resp in responses {
                responses_items.push(from_item(resp).unwrap());
            }

            Some(responses_items)
        } else {
            None
        };

        let unprocessed_keys = if let Some(mut unprocessed) = output.unprocessed_keys {
            let keys = unprocessed.remove(self.table_name).unwrap().keys;
            Some(keys)
        } else {
            None
        };

        Ok((responses, unprocessed_keys))
    }

    /// Update the entry in the database with the given key with the given expression attribute
    /// values, update expression, and optional condition expression.
    pub async fn update(
        &self,
        key: Item,
        values: Item,
        update: String,
        condition: Option<String>,
    ) -> DbResult<T, UpdateItemError> {
        let res = self
            .dynamo
            .update_item(UpdateItemInput {
                expression_attribute_values: Some(values),
                key,
                table_name: self.table_name.to_string(),
                condition_expression: condition,
                update_expression: Some(update),
                return_values: Some("ALL_NEW".to_string()),
                ..Default::default()
            })
            .await?;

        Ok(from_item(res.attributes.unwrap())?)
    }

    /// Scan for entries in the database, starting after the given key, optionally using a filter.
    async fn scan(
        &self,
        exclusive_start_key: Option<Item>,
        filter: Option<String>,
        filter_values: Option<Item>,
    ) -> DbResult<(Vec<T>, Option<Item>), ScanError> {
        let res = self
            .dynamo
            .scan(ScanInput {
                exclusive_start_key,
                table_name: self.table_name.to_string(),
                filter_expression: filter,
                expression_attribute_values: filter_values,
                ..Default::default()
            })
            .await?;

        let ScanOutput {
            items,
            last_evaluated_key,
            ..
        } = res;

        let entries = match items {
            Some(items) => items
                .into_iter()
                .map(from_item)
                .collect::<Result<Vec<T>, _>>()?,
            None => Vec::new(),
        };

        Ok((entries, last_evaluated_key))
    }

    /// Iterate over all the entries in the database, optionally using a filter.
    pub fn iter_entries(
        &self,
        filter: Option<String>,
        filter_values: Option<Item>,
    ) -> impl Stream<Item = DbResult<T, ScanError>> + '_ {
        // This is the easiest way to make a stream (async iterator). Eventually we won't need a macro
        // The stream needs to be pinned in order to be polled. This can either be done when we
        // return the stream, or in the code where we call iter_users.
        // See https://rust-lang.github.io/async-book/04_pinning/01_chapter.html
        // and https://doc.rust-lang.org/std/pin/ for a lot more info.
        Box::pin(try_stream! {
            // Scanning will return a last key scanned until we finish all entries.
            let mut last_key = None;

            // So we loop here until we get a last key that is None again.
            loop {
                let (entries, new_last_key) = self.scan(last_key, filter.clone(), filter_values.clone()).await?;
                last_key = new_last_key;

                for entry in entries {
                    yield entry;
                }

                if last_key.is_none() {
                    break;
                }
            }
        })
    }

    /// Collect the entries from the database into a vector, up to a maximum amount.
    pub async fn collect_entries(
        &self,
        filter: Option<String>,
        filter_values: Option<Item>,
        mut skip: usize,
        length: usize,
    ) -> DbResult<Vec<T>, ScanError> {
        let mut entries = Vec::new();
        let mut last_key = None;

        // So we loop here until we get a last key that is None again.
        loop {
            let (mut batch, new_last_key) = self
                .scan(last_key, filter.clone(), filter_values.clone())
                .await?;
            last_key = new_last_key;

            // If we have more items to skip than we got in this batch. Just skip it
            if skip >= batch.len() {
                skip -= batch.len();
                continue;
            } else {
                // Either take all that remains after skipping. Or take only enough to meet our
                // length. Whichever is less.
                let amount = (batch.len() - skip).min(length - entries.len());
                entries.extend(batch.drain(skip..skip + amount));
                // Since we skipped all we needed, we need to skip 0 more
                skip = 0;
            }

            if last_key.is_none() || entries.len() >= length {
                break;
            }
        }

        Ok(entries)
    }
}
