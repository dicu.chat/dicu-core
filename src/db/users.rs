use crate::db::types::{User, UserIdentity};
use crate::db::user_attributes::UserAttribute;
use crate::db::{Database, DbResult};
use maplit::hashmap;
use rusoto_core::RusotoError;
use rusoto_dynamodb::{
    AttributeDefinition, CreateTableError, CreateTableInput, DeleteItemError, DynamoDb,
    GetItemError, KeySchemaElement, ProvisionedThroughput, PutItemError, ScanError,
    UpdateItemError,
};
use serde_dynamo::{to_attribute_value, to_item};
use tokio_stream::Stream;

impl Database {
    pub(crate) const USERS_TABLE: &'static str = "Users";

    /// Creates a new Users table with identity as the key. If the table was created, Ok(true) is
    /// returned. If the table already existed, Ok(false) is returned. If the table was not created
    /// due to another error, that error is returned.
    pub async fn create_user_table(&self) -> DbResult<bool, CreateTableError> {
        let res = self
            .dynamo
            .create_table(CreateTableInput {
                table_name: Self::USERS_TABLE.to_string(),
                attribute_definitions: vec![AttributeDefinition {
                    attribute_name: "identity".to_string(),
                    attribute_type: "S".to_string(),
                }],
                key_schema: vec![KeySchemaElement {
                    attribute_name: "identity".to_string(),
                    key_type: "HASH".to_string(),
                }],
                provisioned_throughput: Some(ProvisionedThroughput {
                    read_capacity_units: 5000,
                    write_capacity_units: 5000,
                }),
                ..Default::default()
            })
            .await;

        match res {
            Ok(_) => Ok(true),
            // If it failed because the table already exists, lets not return an error.
            Err(RusotoError::Service(CreateTableError::ResourceInUse(_))) => Ok(false),
            Err(e) => Err(e.into()),
        }
    }

    /// Puts a user into the database, overwriting any previous entry with the same identity.
    pub async fn put_user(&self, user: &User) -> DbResult<Option<User>, PutItemError> {
        self.users.put(user).await
    }

    /// Deletes a user given an identity. On success, will return the deleted user structure if it
    /// existed, otherwise None.
    pub async fn delete_user(&self, identity: &str) -> DbResult<Option<User>, DeleteItemError> {
        let key = to_item(UserIdentity {
            identity: identity.to_string(),
        })
        .unwrap();

        self.users.delete(key).await
    }

    /// Gets a user given an identity. Returns None if no user was found with the given identity.
    pub async fn get_user(&self, identity: &str) -> DbResult<Option<User>, GetItemError> {
        let key = to_item(UserIdentity {
            identity: identity.to_string(),
        })
        .unwrap();

        self.users.get(key).await
    }

    /// Changes an arbitrary attribute of the user with the matching identity.
    /// Returns Err if no user with the given identity exists.
    pub async fn update_user_attribute<T: UserAttribute>(
        &self,
        identity: &str,
        attr_value: T::Type,
    ) -> DbResult<User, UpdateItemError> {
        let key = to_item(UserIdentity {
            identity: identity.to_string(),
        })
        .unwrap();

        let values = hashmap! {
            ":val".to_string() => to_attribute_value(attr_value).unwrap()
        };

        // Checks for identity to make sure the user exists, and checks that attribute we're
        // updating exists so that we don't accidentally create a new attribute.
        let cond_expr = format!(
            "attribute_exists(identity) and attribute_exists({})",
            T::NAME
        );
        let update_expr = format!("SET {} = :val", T::NAME);

        self.users
            .update(key, values, update_expr, Some(cond_expr))
            .await
    }

    /// Appends the given state to the states attribute of the user with the matching identity.
    /// Returns Err if no user with the given identity exists.
    pub async fn update_user_append_state(
        &self,
        identity: &str,
        state: &str,
    ) -> DbResult<User, UpdateItemError> {
        let key = to_item(UserIdentity {
            identity: identity.to_string(),
        })
        .unwrap();

        let values = hashmap! {
            ":val".to_string() => to_attribute_value(&[state]).unwrap()
        };

        let condition_expr = "attribute_exists(identity)".to_string();
        let update_expr = "SET states = list_append(states, :val)".to_string();

        self.users
            .update(key, values, update_expr, Some(condition_expr))
            .await
    }

    /// Iterates over all users in the User table.
    pub fn iter_users(&self) -> impl Stream<Item = DbResult<User, ScanError>> + '_ {
        self.users.iter_entries(None, None)
    }
}
