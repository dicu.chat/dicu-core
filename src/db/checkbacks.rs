use crate::db::checkback_attributes::CheckBackAttribute;
use crate::db::types::{CheckBack, CheckBackKwargs};
use crate::db::{Database, DbResult};
use chrono::Utc;
use maplit::hashmap;
use rusoto_core::RusotoError;
use rusoto_dynamodb::{
    AttributeDefinition, CreateTableError, CreateTableInput, DeleteItemError, DynamoDb,
    GlobalSecondaryIndex, KeySchemaElement, Projection, ProvisionedThroughput, PutItemError,
    ScanError, UpdateItemError,
};
use serde_dynamo::to_attribute_value;
use tokio_stream::Stream;
use uuid::Uuid;

impl Database {
    pub(crate) const CHECKBACKS_TABLE: &'static str = "CheckBacks";

    /// Creates a new Checkbacks table with id as the key. If the table was created, Ok(true) is
    /// returned. If the table already existed, Ok(false) is returned. If the table was not created
    /// due to another error, that error is returned.
    pub async fn create_checkbacks_table(&self) -> DbResult<bool, CreateTableError> {
        let res = self
            .dynamo
            .create_table(CreateTableInput {
                table_name: Self::CHECKBACKS_TABLE.to_string(),
                attribute_definitions: vec![
                    AttributeDefinition {
                        attribute_name: "id".to_string(),
                        attribute_type: "S".to_string(),
                    },
                    AttributeDefinition {
                        attribute_name: "send_at".to_string(),
                        attribute_type: "N".to_string(),
                    },
                    AttributeDefinition {
                        attribute_name: "chat_id".to_string(),
                        attribute_type: "S".to_string(),
                    },
                ],

                key_schema: vec![KeySchemaElement {
                    attribute_name: "id".to_string(),
                    key_type: "HASH".to_string(),
                }],

                global_secondary_indexes: Some(vec![GlobalSecondaryIndex {
                    index_name: "chat".to_string(),
                    key_schema: vec![
                        KeySchemaElement {
                            attribute_name: "chat_id".to_string(),
                            key_type: "HASH".to_string(),
                        },
                        KeySchemaElement {
                            attribute_name: "send_at".to_string(),
                            key_type: "RANGE".to_string(),
                        },
                    ],
                    projection: Projection {
                        non_key_attributes: None,
                        projection_type: Some("ALL".to_string()),
                    },
                    provisioned_throughput: Some(ProvisionedThroughput {
                        read_capacity_units: 5000,
                        write_capacity_units: 5000,
                    }),
                }]),

                provisioned_throughput: Some(ProvisionedThroughput {
                    read_capacity_units: 5000,
                    write_capacity_units: 5000,
                }),
                ..Default::default()
            })
            .await;

        match res {
            Ok(_) => Ok(true),
            // If it failed because the table already exists, lets not return an error.
            Err(RusotoError::Service(CreateTableError::ResourceInUse(_))) => Ok(false),
            Err(e) => Err(e.into()),
        }
    }

    /// Creates a new checkback for the user with the given identity and chat_id
    pub async fn put_checkback(
        &self,
        identity: &str,
        chat_id: &str,
        send_in: chrono::Duration,
        kwargs: CheckBackKwargs,
        state: &str,
    ) -> DbResult<Option<CheckBack>, PutItemError> {
        let send_at = chrono::Utc::now() + send_in;

        let checkback = CheckBack {
            id: Uuid::new_v4().to_string(),
            identity: identity.to_string(),
            chat_id: chat_id.to_string(),
            send_at: send_at.timestamp(),
            kwargs,
            state: state.to_string(),
        };

        self.checkbacks.put(&checkback).await
    }

    /// Updates an attribute of the checkback associated with the checkback_id.
    /// Fails if the checkback does not exist
    pub async fn update_checkback_attribute<T: CheckBackAttribute>(
        &self,
        checkback_id: &str,
        attr_value: T::Type,
    ) -> DbResult<CheckBack, UpdateItemError> {
        let key = hashmap! {
            "id".to_string() => to_attribute_value(checkback_id).unwrap()
        };

        let values = hashmap! {
            ":val".to_string() => to_attribute_value(attr_value).unwrap()
        };

        // Checks for identity to make sure the user exists, and checks that attribute we're
        // updating exists so that we don't accidentally create a new attribute.
        let cond_expr = format!("attribute_exists(id) and attribute_exists({})", T::NAME);
        let update_expr = format!("SET {} = :val", T::NAME);

        self.checkbacks
            .update(key, values, update_expr, Some(cond_expr))
            .await
    }

    /// Iterates over all checkbacks schedules to send between two times
    pub fn iter_checkbacks(
        &self,
        start: chrono::DateTime<Utc>,
        stop: chrono::DateTime<Utc>,
    ) -> impl Stream<Item = DbResult<CheckBack, ScanError>> + '_ {
        let filter_values = hashmap! {
            ":start".to_string() => to_attribute_value(start.timestamp()).unwrap(),
            ":stop".to_string() => to_attribute_value(stop.timestamp()).unwrap(),
        };

        let filter = "send_at BETWEEN :start AND :stop".to_string();

        self.checkbacks
            .iter_entries(Some(filter), Some(filter_values))
    }

    /// Returns a pagified amount of checkbacks scheduled to be sent to a chat
    pub async fn query_checkbacks_by_chat(
        &self,
        chat_id: &str,
        page: usize,
        page_size: usize,
    ) -> DbResult<Vec<CheckBack>, ScanError> {
        let filter_values = hashmap! {
            ":chat".to_string() => to_attribute_value(chat_id).unwrap(),
        };
        let filter = "chat_id = :chat".to_string();

        self.checkbacks
            .collect_entries(
                Some(filter),
                Some(filter_values),
                page * page_size,
                page_size,
            )
            .await
    }

    /// Deletes the checkback with the given id
    pub async fn delete_checkback(
        &self,
        checkback_id: &str,
    ) -> DbResult<Option<CheckBack>, DeleteItemError> {
        let key = hashmap! {
            "id".to_string() => to_attribute_value(checkback_id).unwrap()
        };

        self.checkbacks.delete(key).await
    }
}
