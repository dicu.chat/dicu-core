use crate::db::types::{AccessToken, PermissionLevel};
use crate::db::{Database, DbResult};
use maplit::hashmap;
use rusoto_core::RusotoError;
use rusoto_dynamodb::{
    AttributeDefinition, CreateTableError, CreateTableInput, DeleteItemError, DynamoDb,
    GetItemError, GlobalSecondaryIndex, KeySchemaElement, Projection, ProvisionedThroughput,
    PutItemError, ScanError, UpdateItemError,
};
use serde_dynamo::{to_attribute_value, Item};

impl Database {
    pub(crate) const ACCESS_TOKENS_TABLE: &'static str = "AccessTokens";

    /// Creates AccessTokens table. On success returns a boolean indicating if the table was made.
    pub async fn create_access_tokens_table(&self) -> DbResult<bool, CreateTableError> {
        let res = self
            .dynamo
            .create_table(CreateTableInput {
                table_name: Self::ACCESS_TOKENS_TABLE.to_string(),
                attribute_definitions: vec![
                    AttributeDefinition {
                        attribute_name: "token".to_string(),
                        attribute_type: "S".to_string(),
                    },
                    AttributeDefinition {
                        attribute_name: "identity".to_string(),
                        attribute_type: "S".to_string(),
                    },
                ],

                key_schema: vec![KeySchemaElement {
                    attribute_name: "token".to_string(),
                    key_type: "HASH".to_string(),
                }],

                global_secondary_indexes: Some(vec![GlobalSecondaryIndex {
                    index_name: "user_search".to_string(),
                    key_schema: vec![KeySchemaElement {
                        attribute_name: "identity".to_string(),
                        key_type: "HASH".to_string(),
                    }],
                    projection: Projection {
                        non_key_attributes: None,
                        projection_type: Some("ALL".to_string()),
                    },
                    provisioned_throughput: Some(ProvisionedThroughput {
                        read_capacity_units: 5000,
                        write_capacity_units: 5000,
                    }),
                }]),

                provisioned_throughput: Some(ProvisionedThroughput {
                    read_capacity_units: 5000,
                    write_capacity_units: 5000,
                }),
                ..Default::default()
            })
            .await;

        match res {
            Ok(_) => Ok(true),
            // If it failed because the table already exists, lets not return an error.
            Err(RusotoError::Service(CreateTableError::ResourceInUse(_))) => Ok(false),
            Err(e) => Err(e.into()),
        }
    }

    /// Puts an access token into the db. Returns any pre-existing item this overwrote.
    pub async fn put_access_token(
        &self,
        item: &AccessToken,
    ) -> DbResult<Option<AccessToken>, PutItemError> {
        self.access_tokens.put(item).await
    }

    /// Deletes an access token and then resets the permission of the user associated with
    /// the token.
    pub async fn delete_access_token(
        &self,
        token: &str,
    ) -> DbResult<Option<AccessToken>, DeleteItemError> {
        let key = self.make_access_tokens_key(token);

        let access_token = self.access_tokens.delete(key).await?;

        if let Some(item) = &access_token {
            if let Some(id) = &item.identity {
                // TODO: Not sure what the best way to handle errors here is
                self.update_user_attribute::<crate::db::user_attributes::Permissions>(
                    id,
                    PermissionLevel::Anon,
                )
                .await
                .unwrap();
            }
        }

        Ok(access_token)
    }

    /// Checks if an access token has been assigned to anyone yet.
    pub async fn check_access_token(&self, token: &str) -> DbResult<bool, GetItemError> {
        let key = self.make_access_tokens_key(token);

        match self.access_tokens.get(key).await? {
            None => Ok(false),
            Some(access_token) => Ok(access_token.identity.is_none()),
        }
    }

    /// Changes the token owner for a given token, returns the old AccessToken before changes.
    pub async fn update_token_owner(
        &self,
        identity: &str,
        token: &str,
    ) -> DbResult<AccessToken, UpdateItemError> {
        let key = self.make_access_tokens_key(token);

        let values = hashmap! {
            ":id".to_string() => to_attribute_value(identity).unwrap()
        };

        let cond_expr = "attribute_exists(token)".to_string();

        let update_expr = "SET identity = :id".to_string();

        self.access_tokens
            .update(key, values, update_expr, Some(cond_expr))
            .await
    }

    /// Finds all access tokens associated with a user in a paginated manner.
    pub async fn query_access_tokens(
        &self,
        identity: &str,
        page: usize,
        page_size: usize,
    ) -> DbResult<Vec<AccessToken>, ScanError> {
        let filter_values = hashmap! {
            ":id".to_string() => to_attribute_value(identity).unwrap(),
        };
        let filter = "NOT (identity == :id)".to_string();

        self.access_tokens
            .collect_entries(
                Some(filter),
                Some(filter_values),
                page * page_size,
                page_size,
            )
            .await
    }

    fn make_access_tokens_key(&self, token: &str) -> Item {
        hashmap! {
            "token".to_string() => to_attribute_value(token).unwrap(),
        }
    }
}
