use rusoto_mock::{MockCredentialsProvider, MockRequestDispatcher, MultipleMockRequestDispatcher, MockResponseReader, ReadMockResponse};
use rusoto_dynamodb::DynamoDbClient;
use std::fmt::{Debug, Display};
use std::collections::HashMap;
use tokio_stream::StreamExt;

use crate::db::types::{User, Answers, Context, PermissionLevel};
use crate::db::user_attributes::{Permissions, ViaInstance};
use crate::db::{Database, DbError, DbResult};
// Testing utils:

fn patched_db(status: u16, filename: Option<&str>) -> Database {
    // The response is either an empty json object or read from the file:
    let body = filename.map_or("{}".to_string(), |v| MockResponseReader::read_response("tests/static/responses", v));

    Database::with_client(DynamoDbClient::new_with(
        MockRequestDispatcher::with_status(status).with_body(&body),
        MockCredentialsProvider,
        Default::default()
    ))
}

fn patched_db_multiple(responses: Vec<(u16, Option<&str>)>) -> Database {
    let dispatchers: Vec<MockRequestDispatcher> = responses.iter().map(|(status, filename)| {
        let body = filename.map_or("{}".to_string(), |v| MockResponseReader::read_response("tests/static/responses", v));
        MockRequestDispatcher::with_status(*status).with_body(&body)
    }).collect();

    Database::with_client(DynamoDbClient::new_with(
        MultipleMockRequestDispatcher::new(dispatchers),
        MockCredentialsProvider,
        Default::default()
    ))
}

fn validate_error<T: Debug, E: Debug + Display>(resp: DbResult<T, E>, err_name: &str, message: &str) where DbError<E>: Display {
    if let Err(err) = resp {
        // @Robustness(andrew): Error is parsed precisely into some struct/enum (e.g. 'InternalServerError'),
        // but not sure how to do this comparison properly since there seems to be a bunch of nested
        // error types in debug print. But at the same time, this check shouldn't cause any issues.
        assert_eq!(err.to_string(), message, "Wrong error message!");
    } else {
        panic!("Expected bad result ({})! Instead received '{:?}'", err_name, resp);
    }
}

fn validate_stream_error<T: Debug, E: Debug + Display>(res: Option<DbResult<T, E>>, err_name: &str, message: &str) where DbError<E>: Display {
    if let Some(item) = res {
        validate_error(item, err_name, message);
    } else {
        panic!("Expected some result Some(...)! Instead received '{:?}'", res);
    }
}

fn default_test_user() -> User {
    User {
        user_id: "test_id".to_string(),
        service: "test_service".to_string(),
        identity: "1111222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string(),
        via_instance: "11112222333344445555".to_string(),
        first_name: None,
        last_name: None,
        username: None,
        language: "en".to_string(),
        created_at: "2022-02-21T09:33:06.849159+00:00".to_string(),
        location: None,
        last_active: "2022-02-22T09:33:06.849159+00:00".to_string(),
        conversation: None,
        answers: Answers::default(),
        files: HashMap::new(),
        states: Vec::new(),
        permissions: PermissionLevel::Default,
        context: Context::default(),
    }
}

fn default_three_test_users() -> Vec<User> {
    let user = default_test_user();

    let mut user2 = user.clone();
    user2.user_id = "test_id_2".to_string();
    user2.identity = "aaaa222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();

    let mut user3 = user.clone();
    user3.user_id = "test_id_3".to_string();
    user3.identity = "bbbb222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();

    vec![user, user2, user3]
}

// Database.create_user_table:

#[tokio::test]
async fn test_db_create_user_table_created() {
    let db = patched_db(200, Some("create_table_users_success.json"));

    let resp = db.create_user_table().await;

    assert_eq!(resp, Ok(true), "Wrong status during table creation!");
}

#[tokio::test]
async fn test_db_create_user_table_existed() {
    let db = patched_db(400, Some("create_table_users_exists.json"));

    let resp = db.create_user_table().await;

    assert_eq!(resp, Ok(false), "Wrong status during table creation!");
}

#[tokio::test]
async fn test_db_create_user_table_expecting_error_500() {
    let db = patched_db(500, Some("internal_server_error.json"));

    let resp = db.create_user_table().await;

    validate_error(resp, "InternalServerError", "DynamoDB could not process your request");
}

// Database.put_user:

#[tokio::test]
async fn test_db_put_user_success() {
    let db = patched_db(200, None);

    let user = default_test_user();
    let resp = db.put_user(&user).await;

    assert_eq!(resp, Ok(None));
}

#[tokio::test]
async fn test_db_put_user_expecting_error_500() {
    let db = patched_db(500, Some("internal_server_error.json"));

    let user = default_test_user();
    let resp = db.put_user(&user).await;

    validate_error(resp, "InternalServerError", "DynamoDB could not process your request");
}

// Database.delete_user:

#[tokio::test]
async fn test_db_delete_user_success() {
    let db = patched_db(200, Some("test_user_data_attributes.json"));

    let user = default_test_user();
    let resp = db.delete_user(&user.identity).await;

    assert_eq!(resp, Ok(Some(user)));
}

#[tokio::test]
// Since we are returning 'ALL_OLD', this case happens only when the user did not exist.
async fn test_db_delete_user_success_empty() {
    let db = patched_db(200, None);

    let user = default_test_user();
    let resp = db.delete_user(&user.identity).await;

    assert_eq!(resp, Ok(None));
}

#[tokio::test]
async fn test_db_delete_user_expecting_error_corrupted_data() {
    let db = patched_db(200, Some("test_user_data_attributes_corrupted.json"));

    let user = default_test_user();
    let resp = db.delete_user(&user.identity).await;

    validate_error(resp, "Serde error", "missing field `user_id`");
}

#[tokio::test]
async fn test_db_delete_user_expecting_error_400() {
    let db = patched_db(400, Some("resource_users_not_found.json"));

    let user = default_test_user();
    let resp = db.delete_user(&user.identity).await;

    validate_error(resp, "ResourceNotFoundException", "Requested resource not found: Table: Users not found");
}

#[tokio::test]
async fn test_db_delete_user_expecting_error_500() {
    let db = patched_db(500, Some("internal_server_error.json"));

    let user = default_test_user();
    let resp = db.delete_user(&user.identity).await;

    validate_error(resp, "InternalServerError", "DynamoDB could not process your request");
}

// Database.get_user:

#[tokio::test]
async fn test_db_get_user_success() {
    let db = patched_db(200, Some("test_user_data_item.json"));

    let user = default_test_user();
    let resp = db.get_user(&user.identity).await;

    assert_eq!(resp, Ok(Some(user)));
}

#[tokio::test]
// Since we are returning 'ALL_OLD', this case happens only when the user did not exist.
async fn test_db_get_user_success_empty() {
    let db = patched_db(200, None);

    let user = default_test_user();
    let resp = db.get_user(&user.identity).await;

    assert_eq!(resp, Ok(None));
}

#[tokio::test]
async fn test_db_get_user_expecting_error_corrupted_data() {
    let db = patched_db(200, Some("test_user_data_item_corrupted.json"));

    let user = default_test_user();
    let resp = db.get_user(&user.identity).await;

    validate_error(resp, "Serde error", "missing field `user_id`");
}

#[tokio::test]
async fn test_db_get_user_expecting_error_400() {
    let db = patched_db(400, Some("resource_users_not_found.json"));

    let user = default_test_user();
    let resp = db.get_user(&user.identity).await;

    validate_error(resp, "ResourceNotFoundException", "Requested resource not found: Table: Users not found");
}

#[tokio::test]
async fn test_db_get_user_expecting_error_500() {
    let db = patched_db(500, Some("internal_server_error.json"));

    let user = default_test_user();
    let resp = db.get_user(&user.identity).await;

    validate_error(resp, "InternalServerError", "DynamoDB could not process your request");
}

// Database.update_user_attribute:

#[tokio::test]
async fn test_db_update_user_attribute_success() {
    let mut user = default_test_user();

    {
        let db = patched_db(200, Some("update_user_attribute_success_1.json"));

        let resp = db.update_user_attribute::<Permissions>(&user.identity, PermissionLevel::Admin).await;

        // Updating in-memory version for comparison.
        user.permissions = PermissionLevel::Admin;

        assert_eq!(resp, Ok(user.clone()));
    }

    {
        let db = patched_db(200, Some("update_user_attribute_success_2.json"));

        let resp = db.update_user_attribute::<ViaInstance>(&user.identity, "66667777888899990000".to_string()).await;

        // Updating in-memory version for comparison.
        user.via_instance = "66667777888899990000".to_string();

        assert_eq!(resp, Ok(user.clone()));
    }
}

#[tokio::test]
async fn test_db_update_user_attribute_condition_failed() {
    let db = patched_db(400, Some("conditional_check_failed.json"));

    let user = default_test_user();
    let resp = db.update_user_attribute::<Permissions>(&user.identity, PermissionLevel::Admin).await;

    validate_error(resp, "ConditionalCheckFailedException", "Failed condition.");
}

#[tokio::test]
async fn test_db_update_user_attribute_expecting_error_400() {
    let db = patched_db(400, Some("resource_users_not_found.json"));

    let user = default_test_user();
    let resp = db.update_user_attribute::<Permissions>(&user.identity, PermissionLevel::Admin).await;

    validate_error(resp, "ResourceNotFoundException", "Requested resource not found: Table: Users not found");
}

#[tokio::test]
async fn test_db_update_user_attribute_expecting_error_500() {
    let db = patched_db(500, Some("internal_server_error.json"));

    let user = default_test_user();
    let resp = db.update_user_attribute::<Permissions>(&user.identity, PermissionLevel::Admin).await;

    validate_error(resp, "InternalServerError", "DynamoDB could not process your request");
}

// Database.update_user_append_state:

#[tokio::test]
async fn test_db_update_user_append_state_success() {
    let mut user = default_test_user();

    {
        let db = patched_db(200, Some("update_user_append_state_success_1.json"));

        let resp = db.update_user_append_state(&user.identity, "QAState").await;

        // Updating in-memory version for comparison.
        user.states.push("QAState".to_string());

        assert_eq!(resp, Ok(user.clone()));
    }

    {
        let db = patched_db(200, Some("update_user_append_state_success_2.json"));

        let resp = db.update_user_append_state(&user.identity, "AIState").await;

        // Updating in-memory version for comparison.
        user.states.push("AIState".to_string());

        assert_eq!(resp, Ok(user.clone()));
    }
}

#[tokio::test]
async fn test_db_update_user_append_state_condition_failed() {
    let db = patched_db(400, Some("conditional_check_failed.json"));

    let user = default_test_user();
    let resp = db.update_user_append_state(&user.identity, "QAState").await;

    validate_error(resp, "ConditionalCheckFailedException", "Failed condition.");
}

#[tokio::test]
async fn test_db_update_user_append_state_expecting_error_400() {
    let db = patched_db(400, Some("resource_users_not_found.json"));

    let user = default_test_user();
    let resp = db.update_user_append_state(&user.identity, "QAState").await;

    validate_error(resp, "ResourceNotFoundException", "Requested resource not found: Table: Users not found");
}

#[tokio::test]
async fn test_db_update_user_append_state_expecting_error_500() {
    let db = patched_db(500, Some("internal_server_error.json"));

    let user = default_test_user();
    let resp = db.update_user_append_state(&user.identity, "QAState").await;

    validate_error(resp, "InternalServerError", "DynamoDB could not process your request");
}

// Database.iter_users:

#[tokio::test]
async fn test_db_iter_users_success_empty() {
    let db = patched_db(200, Some("db_scan_users_success_empty.json"));

    let mut stream = db.iter_users();

    assert_eq!(stream.next().await, None);
}

#[tokio::test]
async fn test_db_iter_users_success_single() {
    let db = patched_db(200, Some("db_scan_users_success_single.json"));

    let mut stream = db.iter_users();

    assert_eq!(stream.next().await, Some(Ok(default_test_user())));
    assert_eq!(stream.next().await, None);
}

#[tokio::test]
async fn test_db_iter_users_success_multiple() {
    let db = patched_db(200, Some("db_scan_users_success_multiple.json"));

    let users = default_three_test_users();
    let mut stream = db.iter_users();

    assert_eq!(stream.next().await, Some(Ok(users[0].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[1].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[2].clone())));
    assert_eq!(stream.next().await, None);
}

#[tokio::test]
async fn test_db_iter_users_success_paginated() {
    let db = patched_db_multiple(vec![
        (200, Some("db_scan_users_success_many_part_1.json")),
        (200, Some("db_scan_users_success_many_part_2.json")),
        (200, Some("db_scan_users_success_many_part_3.json")),
    ]);

    let mut users = default_three_test_users();
    let mut stream = db.iter_users();

    // Page 1

    assert_eq!(stream.next().await, Some(Ok(users[0].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[1].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[2].clone())));

    // Page 2

    users[0].user_id = "test_id_4".to_string();
    users[0].identity = "cccc222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();
    users[1].user_id = "test_id_5".to_string();
    users[1].identity = "dddd222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();
    users[2].user_id = "test_id_6".to_string();
    users[2].identity = "eeee222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();

    assert_eq!(stream.next().await, Some(Ok(users[0].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[1].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[2].clone())));

    // Page 3

    users[0].user_id = "test_id_7".to_string();
    users[0].identity = "abcd222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();
    users[1].user_id = "test_id_8".to_string();
    users[1].identity = "cdef222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();
    users[2].user_id = "test_id_9".to_string();
    users[2].identity = "abef222233334444555566667777888899990000aaaabbbbccccddddeeeefffffff".to_string();

    assert_eq!(stream.next().await, Some(Ok(users[0].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[1].clone())));
    assert_eq!(stream.next().await, Some(Ok(users[2].clone())));

    assert_eq!(stream.next().await, None);
}

#[tokio::test]
async fn test_db_iter_users_expecting_error_corrupted_data() {
    let db = patched_db(200, Some("db_scan_users_success_multiple_corrupted.json"));

    let mut stream = db.iter_users();

    validate_stream_error(stream.next().await, "Serde error", "missing field `user_id`");
    assert_eq!(stream.next().await, None);
}

#[tokio::test]
async fn test_db_iter_users_expecting_error_400() {
    let db = patched_db(400, Some("resource_users_not_found.json"));

    let mut stream = db.iter_users();

    validate_stream_error(stream.next().await, "ResourceNotFoundException", "Requested resource not found: Table: Users not found");
    assert_eq!(stream.next().await, None);
}

#[tokio::test]
async fn test_db_iter_users_expecting_error_500() {
    let db = patched_db(500, Some("internal_server_error.json"));

    let mut stream = db.iter_users();

    validate_stream_error(stream.next().await, "InternalServerError", "DynamoDB could not process your request");
    assert_eq!(stream.next().await, None);
}
