use crate::db::types::StringItem;
use crate::db::{Database, DbResult};
use maplit::hashmap;
use rusoto_core::RusotoError;
use rusoto_dynamodb::{
    AttributeDefinition, BatchGetItemError, BatchWriteItemError, CreateTableError,
    CreateTableInput, DynamoDb, GetItemError, KeySchemaElement, ProvisionedThroughput,
    PutItemError, ScanError,
};
use serde_dynamo::{to_attribute_value, Item};
use tokio_stream::Stream;

impl Database {
    pub(crate) const STRING_ITEMS_TABLE: &'static str = "StringItems";

    /// Create string items table with language and string_key as the key.
    /// Boolean indicates if the table was created.
    pub async fn create_string_items_table(&self) -> DbResult<bool, CreateTableError> {
        let res = self
            .dynamo
            .create_table(CreateTableInput {
                table_name: Self::STRING_ITEMS_TABLE.to_string(),
                attribute_definitions: vec![
                    AttributeDefinition {
                        attribute_name: "language".to_string(),
                        attribute_type: "S".to_string(),
                    },
                    AttributeDefinition {
                        attribute_name: "string_key".to_string(),
                        attribute_type: "S".to_string(),
                    },
                ],

                key_schema: vec![
                    KeySchemaElement {
                        attribute_name: "language".to_string(),
                        key_type: "HASH".to_string(),
                    },
                    KeySchemaElement {
                        attribute_name: "string_key".to_string(),
                        key_type: "RANGE".to_string(),
                    },
                ],

                global_secondary_indexes: None,

                provisioned_throughput: Some(ProvisionedThroughput {
                    read_capacity_units: 5000,
                    write_capacity_units: 5000,
                }),
                ..Default::default()
            })
            .await;

        match res {
            Ok(_) => Ok(true),
            // If it failed because the table already exists, lets not return an error.
            Err(RusotoError::Service(CreateTableError::ResourceInUse(_))) => Ok(false),
            Err(e) => Err(e.into()),
        }
    }

    /// Put a string item into the table, returning any overwritten item.
    pub async fn put_translation(
        &self,
        item: &StringItem,
    ) -> DbResult<Option<StringItem>, PutItemError> {
        self.string_items.put(item).await
    }

    /// Put many string items into the table at once, returning any items that failed to be put.
    pub async fn bulk_put_translations(
        &self,
        items: &[StringItem],
    ) -> DbResult<Option<Vec<StringItem>>, BatchWriteItemError> {
        self.string_items.batch_put(items).await
    }

    /// Get the item corresponding to the given key if it exists.
    pub async fn get_translation(
        &self,
        lang: &str,
        key: &str,
    ) -> DbResult<Option<StringItem>, GetItemError> {
        let key = self.make_string_item_key(lang, key);
        self.string_items.get(key).await
    }

    /// Try and get all the items corresponding to all the keys provided.
    /// Keys are represented as a tuple of (language, string_key).
    /// Returns a tuple of results and unprocessed keys.
    pub async fn bulk_get_translations(
        &self,
        items: &[(String, String)],
    ) -> DbResult<(Option<Vec<StringItem>>, Option<Vec<Item>>), BatchGetItemError> {
        let keys = items
            .iter()
            .map(|(lang, key)| self.make_string_item_key(lang, key))
            .collect::<Vec<Item>>();

        self.string_items.batch_get(keys).await
    }

    /// Iterate over all string keys in the table.
    pub async fn iter_translations(
        &self,
    ) -> impl Stream<Item = DbResult<StringItem, ScanError>> + '_ {
        self.string_items.iter_entries(None, None)
    }

    fn make_string_item_key(&self, lang: &str, key: &str) -> Item {
        hashmap! {
            "language".to_string() => to_attribute_value(lang).unwrap(),
            "string_key".to_string() => to_attribute_value(key).unwrap(),
        }
    }
}
