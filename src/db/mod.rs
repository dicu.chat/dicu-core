pub mod checkback_attributes;
pub mod types;
pub mod user_attributes;

mod access_tokens;
mod checkbacks;
mod external_services;
mod sessions;
mod string_items;
mod table;
mod users;

use crate::db::table::Table;
use crate::db::types::{AccessToken, CheckBack, ExternalService, Session, StringItem, User};
use rusoto_core::{Region, RusotoError};
use rusoto_dynamodb::DynamoDbClient;
use std::fmt::{Debug, Formatter};

#[derive(Debug, PartialEq)]
/// A database error that could have occurred from a deserialization error or a result from the
/// database query.
pub enum DbError<E: Debug> {
    Serde(serde_dynamo::Error),
    Rusoto(RusotoError<E>),
}

impl<E: std::error::Error + 'static> std::fmt::Display for DbError<E> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            DbError::Serde(s) => std::fmt::Display::fmt(s, f),
            DbError::Rusoto(r) => std::fmt::Display::fmt(r, f),
        }
    }
}

impl<E: std::error::Error + 'static> std::error::Error for DbError<E> {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            DbError::Serde(s) => Some(s),
            DbError::Rusoto(r) => Some(r),
        }
    }
}

impl<E: Debug> From<serde_dynamo::Error> for DbError<E> {
    fn from(e: serde_dynamo::Error) -> Self {
        DbError::Serde(e)
    }
}

impl<E: Debug> From<RusotoError<E>> for DbError<E> {
    fn from(e: RusotoError<E>) -> Self {
        DbError::Rusoto(e)
    }
}

type DbResult<T, E> = Result<T, DbError<E>>;

/// Wrapper around the dynamodb interface to provide simple accessors for the server tables.
pub struct Database {
    dynamo: DynamoDbClient,
    checkbacks: Table<CheckBack>,
    users: Table<User>,
    access_tokens: Table<AccessToken>,
    sessions: Table<Session>,
    string_items: Table<StringItem>,
    external_services: Table<ExternalService>,
}

impl Database {
    /// Create new database given a database url and name.
    pub fn new(database_url: &str, name: &str) -> Database {
        let region = Region::Custom {
            name: name.to_string(),
            endpoint: database_url.to_string(),
        };

        Self::with_client(DynamoDbClient::new(region))
    }

    /// Create new database using a pre-made dynamo client.
    pub fn with_client(dynamo: DynamoDbClient) -> Database {
        let checkbacks = Table::new(dynamo.clone(), Self::CHECKBACKS_TABLE);
        let users = Table::new(dynamo.clone(), Self::USERS_TABLE);
        let access_tokens = Table::new(dynamo.clone(), Self::ACCESS_TOKENS_TABLE);
        let sessions = Table::new(dynamo.clone(), Self::SESSIONS_TABLE);
        let string_items = Table::new(dynamo.clone(), Self::STRING_ITEMS_TABLE);
        let external_services = Table::new(dynamo.clone(), Self::EXTERNAL_SERVICES_TABLE);

        Database {
            dynamo,
            checkbacks,
            users,
            access_tokens,
            sessions,
            string_items,
            external_services,
        }
    }
}

#[cfg(test)]
mod users_test;
