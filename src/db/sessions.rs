use crate::db::types::Session;
use crate::db::{Database, DbResult};
use maplit::hashmap;
use rusoto_core::RusotoError;
use rusoto_dynamodb::{
    AttributeDefinition, CreateTableError, CreateTableInput, DynamoDb, GetItemError,
    KeySchemaElement, ProvisionedThroughput, PutItemError, ScanError,
};
use serde_dynamo::to_attribute_value;
use tokio_stream::Stream;

impl Database {
    pub(crate) const SESSIONS_TABLE: &'static str = "Sessions";

    /// Creates Sessions table with name as the key
    /// Boolean indicates if the table was created.
    pub async fn create_sessions_table(&self) -> DbResult<bool, CreateTableError> {
        let res = self
            .dynamo
            .create_table(CreateTableInput {
                table_name: Self::SESSIONS_TABLE.to_string(),
                attribute_definitions: vec![AttributeDefinition {
                    attribute_name: "name".to_string(),
                    attribute_type: "S".to_string(),
                }],

                key_schema: vec![KeySchemaElement {
                    attribute_name: "name".to_string(),
                    key_type: "HASH".to_string(),
                }],

                global_secondary_indexes: None,

                provisioned_throughput: Some(ProvisionedThroughput {
                    read_capacity_units: 5000,
                    write_capacity_units: 5000,
                }),
                ..Default::default()
            })
            .await;

        match res {
            Ok(_) => Ok(true),
            // If it failed because the table already exists, lets not return an error.
            Err(RusotoError::Service(CreateTableError::ResourceInUse(_))) => Ok(false),
            Err(e) => Err(e.into()),
        }
    }

    /// Puts a session into the table, returning any overwritten entry with the same key.
    pub async fn put_session(&self, item: &Session) -> DbResult<Option<Session>, PutItemError> {
        self.sessions.put(item).await
    }

    /// Gets the session with the given name if found.
    pub async fn get_session(
        &self,
        instance_name: &str,
    ) -> DbResult<Option<Session>, GetItemError> {
        let key = hashmap! {
            "name".to_string() => to_attribute_value(instance_name).unwrap()
        };

        self.sessions.get(key).await
    }

    /// Iterates through all sessions
    pub fn iter_sessions(&self) -> impl Stream<Item=DbResult<Session, ScanError>> + '_ {
        self.sessions.iter_entries(None, None)
    }
}
