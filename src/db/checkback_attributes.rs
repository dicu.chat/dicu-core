use crate::db::types::CheckBackKwargs;
use serde::Serialize;

pub trait CheckBackAttribute {
    const NAME: &'static str;
    type Type: Serialize;
}

pub enum Identity {}
pub enum ChatId {}
pub enum SendAt {}
pub enum Kwargs {}
pub enum State {}

impl CheckBackAttribute for Identity {
    const NAME: &'static str = "identity";
    type Type = String;
}

impl CheckBackAttribute for ChatId {
    const NAME: &'static str = "chat_id";
    type Type = String;
}

impl CheckBackAttribute for SendAt {
    const NAME: &'static str = "send_at";
    type Type = i64;
}

impl CheckBackAttribute for Kwargs {
    const NAME: &'static str = "kwargs";
    type Type = CheckBackKwargs;
}

impl CheckBackAttribute for State {
    const NAME: &'static str = "state";
    type Type = String;
}
