use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub(crate) struct UserIdentity {
    pub identity: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct User {
    pub user_id: String,
    pub service: String,
    pub identity: String,
    pub via_instance: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub username: Option<String>,
    pub language: String,
    pub created_at: String,
    pub location: Option<String>,
    pub last_active: String,
    pub conversation: Option<String>,
    pub answers: Answers,
    pub files: HashMap<String, String>,
    pub states: Vec<String>,
    pub permissions: PermissionLevel,
    pub context: Context,
}

// Hasan: Similar to context below, just a copy of what we're using or saving in python for now
#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct Answers {
    pub qa: Option<QaAnswers>,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct QaAnswers {
    pub curr_q: Option<String>,
    pub qa_results: HashMap<String, String>,
    pub qa_history: Vec<String>,
    pub multichoice_cache: HashMap<String, String>,
    pub score: f64,
    pub stored_vars: StoredVars,
    pub save_answer: bool,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct StoredVars {
    pub back_ignore_partials_leaves: HashMap<String, bool>,
    pub skip_timeouts: HashMap<String, f64>,
    pub flow_config: FlowConfig,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct FlowConfig {
    pub default_stop: bool,
}

// Hasan: Very rough pass to try and grab all variables that were referenced in user["context"].
// A clean up will be done as the rewrite of the server progresses and we see what is used or not used.
// For now, this is useful for being able to read existing database entries.
#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct Context {
    // General
    pub language: Option<String>,
    pub expecting_voice: Option<bool>,
    pub enable_end_message: Option<bool>,
    pub bq_state: Option<f64>,

    // ai_state.py
    pub enable_ai_message: Option<bool>,
    pub random_ai_model: Option<bool>,
    pub current_ai_model: Option<String>,
    pub preferred_ai_model: Option<String>,
    pub aistate: Option<AiStateHistory>,
    pub aistate_talked: Option<bool>,
    pub message_ai_limit: Option<f64>,

    // authorization_state.py
    pub tokens_menu: Option<Menu>,

    // language_detection_state.py
    pub language_state: Option<f64>,
    pub lang_code: Option<String>,
    pub language_obj: Option<HashMap<String, String>>,

    // qa_state.py
    pub current_flow_name: Option<String>,
    pub remind_q: Option<f64>,

    // reminder.py
    pub reminder: Option<Reminder>,
    pub jira_multichoice: Option<Vec<String>>,
    pub jira_projects: Option<HashMap<String, String>>,
    pub reminder_menu: Option<Menu>,

    // start_state.py
    pub bad_permissions_warned: Option<f64>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct AiStateHistory {
    pub history: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Reminder {
    #[serde(rename = "type")]
    pub type_: String,
    pub jira: Option<JiraReminder>,
    pub custom: Option<CustomReminder>,
    pub period: String,
    pub time: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct JiraReminder {
    pub statuses: Vec<String>,
    pub projects: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct CustomReminder {
    pub title: String,
    pub body: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Menu {
    pub page: f64,
    pub page_size: f64,
    pub total_approx: f64,
    pub current_page: HashMap<String, String>,
}

#[derive(Debug, Serialize_repr, Deserialize_repr, PartialEq, Copy, Clone)]
#[repr(u32)]
pub enum PermissionLevel {
    Anon = 0,
    Default = 1,
    Admin = 2,
    Max = 5,
    // Why 5?
    #[serde(other)]
    Unknown,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct CheckBack {
    pub id: String,
    pub identity: String,
    pub chat_id: String,
    pub send_at: i64,
    pub kwargs: CheckBackKwargs,
    pub state: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct CheckBackKwargs {
    pub title: String,
    pub period_days: Option<u64>,
    pub type_: String,
    pub projects: Option<Vec<String>>,
    pub statuses: Option<Vec<String>>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct AccessToken {
    pub token: String,
    pub identity: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Session {
    pub name: String,
    pub token: String,
    pub url: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct StringItem {
    pub language: String,
    pub string_key: String,
    pub content_hash: String,
    pub text: String,
    pub manual: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct ExternalService {
    pub identity: String,
    pub service: String,
    pub url: String,
    pub username: String,
    pub token: String,
    pub credentials: Credentials,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum Credentials {
    Google {
        token: String,
        refresh_token: String,
        token_uri: String,
        client_id: String,
        client_secret: String,
        scopes: String,
    },
}
