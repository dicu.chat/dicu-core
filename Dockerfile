FROM clux/muslrust as builder-base
WORKDIR /usr/src/app
# Copying config/build files.
COPY src src
COPY Cargo.toml .
COPY Cargo.lock .
RUN rustup target add x86_64-unknown-linux-musl


FROM builder-base as builder-main
# Building the program.
RUN cargo build --profile=release --locked --target x86_64-unknown-linux-musl


# Note(andrew): We can't build from scratch without much hustle, because
#     program will fail on runtime due to lack of CA files:
#
#         thread 'main' panicked at 'no CA certificates found', .../hyper-rustls-0.22.1/src/connector.rs:45:13
#
#     So, instead, lets just build from alpine base (extra 5MB, total 17MB). If there
#     is some dead simple solution to this, we might consider switching.  @Robustness @Optimization
FROM alpine:3.15 as main
# Copying compiled executable from the 'builder'.
COPY --from=builder-main /usr/src/app/target/x86_64-unknown-linux-musl/release/dicu_core .
# Running binary.
ENTRYPOINT ["./dicu_core"]


# Note(andrew): The size of this image is over 2.3GB, but we can't do anything
#     about it, because we need all the generated debug info and cargo to run
#     tests properly.
FROM builder-base as test
# Copying the rest of test files
COPY tests tests
# Building the program in testing mode
RUN cargo test --no-run
# Running binary.
ENTRYPOINT ["cargo", "test"]
